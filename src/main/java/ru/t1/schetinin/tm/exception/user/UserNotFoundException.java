package ru.t1.schetinin.tm.exception.user;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}